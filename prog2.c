#include <stdio.h>
#include <string.h>

void reverse(char *st){
	size_t length = strlen(st);
	printf("\nst: %s", st);
	printf("\n*st: %c", *st);
	char *ch = st + length - 1;
	printf("\nch: %s", ch);
	printf("\n*ch: %c", *ch);	
	printf("\nInverted String: ");
	while (ch >= st){
		printf("%c", *ch);
		ch = ch - 1;
	}
	puts("");
}

int main(int argc, char **argv){
	for (int i = 1; i < argc; i++)
		reverse(argv[i]);
}
