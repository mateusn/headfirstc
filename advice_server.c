#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <arpa/inet.h>

void error(const char *msg)
{
	fprintf(stderr, "%s: %s\n", msg, strerror(errno));
	exit(1);
}

const char *messages[] = {
"1: Interesting\r\n", "2: 42\r\n", "3: ayy\r\n", "4: lmao\r\n", "5: kek\r\n"
};

int main()
{
	int listener_d = socket(PF_INET, SOCK_STREAM, 0); //listener declaration
	if (listener_d == -1)
		error("Failed to open socket");

	//socket declaration
	struct sockaddr_in name;
	name.sin_family = PF_INET;
	name.sin_port = (in_port_t)htons(30000);
	name.sin_addr.s_addr = htonl(INADDR_ANY);

	//binding
	int c = bind(listener_d, (struct sockaddr *)&name, sizeof(name));
	if (c == -1)
		error("Failed to bind port");

	//listening
	if (listen(listener_d, 10) == -1)
		error("Failed to listen");

	
	puts("Waiting for connection...");
	while(1)
	{
		//connecting with a client
		struct sockaddr_storage client_addr;
		unsigned int address_size = sizeof(client_addr);
		int connect_d = accept(listener_d, (struct sockaddr *)&client_addr, &address_size);
		
		//selecting a message
		char *msg = (char *)messages[rand() % 5];

		//sending it
		if (send(connect_d, msg, strlen(msg), 0) == -1)
			error("Failed to send message");


		close(connect_d);

	}
	return 0;
}
