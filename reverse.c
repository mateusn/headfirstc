#include <stdio.h>
#include <string.h>

void reverse(char *str){
	size_t length = strlen(str);
	//gets string length
	char *a = str + length - 1;

//	printf("----%--s\n", *a);
	while (a >= str){
		printf("%c", *a);

		a = a - 1;
	}
	puts("");
}

int main(int argc, char **argv){
	for (int i = 1; i < argc; i++)
		reverse(argv[i]);
}
